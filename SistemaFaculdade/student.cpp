#include "student.h"
#include <iostream>


Student::Student() {}

Student::Student(QString name, QString email, QString address, QString cpf, QString rg) {
   setName(name);
   setEmail(email);
   setAddress(address);
   setCpf(cpf);
   setRg(rg);
}

QString Student::getRegistration() {
    return registration;
}

QString Student::getStartDate() {
    return startDate;
}

StudentStatusEnum Student::getStatus() {
    return status;
}

void Student::printData() {

}
