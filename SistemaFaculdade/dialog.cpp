#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::setData(College college) {
    ui->textEdit->clear();
    QString data = QString::fromStdString("Faculdade: \n") +
            QString::fromStdString("\nNome: ") + college.getName() +
            QString::fromStdString("\nEndereço: ") + college.getAddress() +
            QString::fromStdString("\nCNPJ: ") + college.getCnpj() +
            QString::fromStdString("\n\nCursos: ");
    for (Course course : college.getCourses()) {
        data += QString::fromStdString("\nCódigo: ") + course.getCode() +
                QString::fromStdString("\nNome: ") + course.getName() +
                QString::fromStdString("\nDescrição: ") + course.getDescription() +
                QString::fromStdString("\nCarga horária: ") + QString::number(course.getWorkload());

        this->appendCourseTeacherData(&data, course);
    }

    ui->textEdit->setText(data);
}

void Dialog::appendCourseTeacherData(QString *data, Course course) {
    for (Teacher teacher : course.getTeachers()) {
        data->append(teacher.getName()).append(teacher.getCpf());
    }
}
