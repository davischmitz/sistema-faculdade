#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "college.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void setData(College);

private:
    Ui::Dialog *ui;

    void appendCourseTeacherData(QString *, Course);
};

#endif // DIALOG_H
