#include "subjectregisterservice.h"

SubjectRegisterService::SubjectRegisterService() {}

void SubjectRegisterService::registerToCourse(QList<Course> *courses, QString courseName, Subject subject) {
    for (int i = 0; i < courses->size(); i++) {
        qDebug() << (*courses)[i].getName();
        qDebug() << courseName;
        if ((*courses)[i].getName() == courseName) {
            qDebug() << "Registrando" << subject.getName();
            qDebug() << "Registrando" << subject.getBibliography();
            (*courses)[i].registerSubject(subject);
        }
    }
}
